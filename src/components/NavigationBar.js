import React, {Component} from 'react';

class NavigationBar extends React.Component{
	
	render(){
		return (
			<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			    <a class="navbar-brand"  href="/">Voter</a>
			    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
			        <span class="navbar-toggler-icon"></span>
			    </button>

			    <div class="collapse navbar-collapse" id="navbarColor02">
			        <ul class="navbar-nav mr-auto">
			            <li class="nav-item">
			                <a class="nav-link" href="/">Home</a>
			            </li>
			            <li class="nav-item">
			                <a class="nav-link" href="/poll">New Poll</a>
			            </li>
			            <li class="nav-item" >
			                <a class="nav-link" href="/{id}/polls(id=${userId})}">My Polls</a>
			            </li>
			        </ul>
			        <ul class="navbar-nav">
			            <li class="nav-item">
			                <a class="nav-link" href="/login">Sign in</a>
			            </li>
			            <li class="nav-item">
			                <a class="nav-link" href="@{/register}">Sign Up</a>
			            </li>
			            <li class="nav-item dropdown" >
			                <a class="nav-link dropdown-toggle" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#" />
			                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
			                    <div class="dropdown-item" style="padding:0px;">
			                        <img  height="100px" id="profile-picture" src="@{/profile/avatar}"/>
			                    </div>
			                    <a class="dropdown-item"  href="/avatar" >Set Profile Picture</a>
			                </div>
			            </li>
			            <li class="nav-item"  >
			                <a class="nav-link" href="/logout" >Sign out</a>
			            </li>
			        </ul>

			    </div>
			</nav>
		)
	}
}

export default NavigationBar;