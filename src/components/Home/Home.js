import React from 'react';
import {PollItem} from './../PollItem';

class Home extends React.Component{

	onPollClicked(){}
	onPollOwnerClicked(){}

	render(){

		const polls = [{
				owner:{
					id: 1,
					fullName: "Waqqas Sheikh"
				},
				title: 'Who you gonna call',
				id: 1
		}];

		const pollItems = polls.map((poll)=>(
			<PollItem 
				key = {poll.id}
				title = {poll.title}
				ownerName={poll.owner.fullName}
				onPollClicked={()=>this.onPollClicked(poll)}
				onPollOwnerClicked={()=>this.onPollOwnerClicked(poll.owner)}
				/>
		));

		return (
			<div className="container">
		        <div className="row justify-content-center">
		            <div className="col-6">
		                <h2 className="text-center">Recent Polls</h2>
		                {pollItems}
		            </div>
		        </div>
		    </div>
		)
	}
}

export default Home;