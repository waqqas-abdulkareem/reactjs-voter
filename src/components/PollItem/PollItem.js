import React from 'react';

const PollItem = (props) => {

	let {
		ownerName,
		title,
		onPollOwnerClicked,
		onPollClicked
	} = props;

	return (
		<div className="card text-white bg-secondary mb-3">
            <div className="card-header"  onClick={onPollOwnerClicked} >{ownerName}</div>
            <div className="card-body" onClick={onPollClicked}>
                <h4 className="card-title">{title}</h4>
            </div>
        </div>
	)
};

export default PollItem;