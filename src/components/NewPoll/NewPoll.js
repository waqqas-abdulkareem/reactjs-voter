import React from 'react';
import {connect} from 'react-redux';
import {
	UPDATE_TEXT_FIELD,
	UPDATE_OPTION_FIELD,
	ADD_OPTION_FIELD
} from './../../constants/actionTypes';

const TitleSection = (props) => {

	let {
		title,
		value,
		placeholder,	
		minLength,
		maxLength,
		validationError,
		onTitleChanged
	} = props;

	let validationField;
	if (validationError) {
		validationField = (<div className="invalid-feedback">{validationError}</div>);
	}

	return (
	<div className="form-group">
        <label >{title}</label>
        <input type="text" 
        		className="form-control" 
               	placeholder={placeholder}
               	maxLength={maxLength || 255}
               	minLength={minLength || 10}
               	value={value}
               	onChange={onTitleChanged}
               	required/>
        {validationField}
    </div>)
}

const OptionsSection = (props) => {

	let {
		title,
		options,
		onOptionsChanged,
		validationError
	} = props;

	let validationField;
	if (validationError) {
		validationField = (<div className="d-block invalid-feedback">{validationError}</div>);
	}

	const optionFields = (options || ["",""]).map((option,i)=>{
		return (<input 
			type="text" 
			key={i}
			className="form-control option-item"
            value={option} 
            onChange={(ev)=>onOptionsChanged(i,ev.target.value)}/>)
	})

	return (
	<div className="form-group">
        <label>{title}</label>
        <div id="options">
            {optionFields}
        </div>

    </div>)
}

const mapStateToProps = (state)=>{
	return {
		...state.newPoll
	}
}

const mapDispatchToProps = (dispatch)=>{
	return {
		onTitleChanged:(title)=>{
			dispatch({type: UPDATE_TEXT_FIELD, form: 'newPoll', key: 'title',value: title});
		},
		onOptionsChanged:(index,value)=>{
			dispatch({type: UPDATE_OPTION_FIELD, index, value});
		},
		onSubmitNewPoll:(newPoll)=>{
			alert(`${JSON.stringify(newPoll, undefined,2)}`)
		},
		onAddNewOption:()=>{
			dispatch({type:ADD_OPTION_FIELD})
		}
	}
}

class NewPoll extends React.Component{

	constructor(props){
		super(props);
		this.onTitleChanged = (ev)=>{
			this.props.onTitleChanged(ev.target.value);
		}
		this.onOptionsChanged = (i,v)=>{
			this.props.onOptionsChanged(i,v);
		}
		this.onSubmitNewPoll = ()=>{
			this.props.onSubmitNewPoll({
				title: this.props.title,
				options: this.props.options
			})
		}
		this.onAddNewOption = (ev)=>{
			this.props.onAddNewOption()
		}
	}

	render(){

		let {
			title,
			options
		} = this.props

		return (
		<div className="container">
	        <div className="row justify-content-center">
	            <div id="new_poll" className="col-4">
	                <h2> New Poll</h2>
	                <div>
	                    <TitleSection
	                    	id="title_section"
	                    	title="Title"
	                    	value={title}
	                    	placeholder="Enter poll title"
	                    	maxLength="255"
	                    	minLength="10"
	                    	validationError=""
	                    	onTitleChanged={this.onTitleChanged}
	                    />
	                    <OptionsSection
	                    	id="options_section"
	                    	title="Options"
	                    	options={options}
	                    	validationError=""
	                    	onOptionsChanged={this.onOptionsChanged}
	                    />
	                    <button type="submit" className="btn btn-primary" onClick={this.onSubmitNewPoll}>Submit</button>
	                    <button type="button" className="btn btn-secondaru" onClick={this.onAddNewOption}>Add Option</button>
	                </div>
	            </div>
	        </div>
        </div>);
	}
}

export default connect(mapStateToProps,mapDispatchToProps)(NewPoll);