import React from 'react';
import {Switch, Route} from 'react-router-dom';
import {Home} from './../Home';
import {Login} from './../Login';
import {Register} from './../Register';
import {NewPoll} from './../NewPoll';
import {MyPolls} from './../MyPolls';

class Main extends React.Component {
  render() {
  	return (
  		<Switch>
		    <Route exact path='/' component={Home}/>
        <Route exact path="/poll" component={NewPoll}/>
        <Route exact path="/polls" component={MyPolls}/>
		    <Route path="/login" component={Login}/>
		    <Route path="/register" component={Register}/>
		  </Switch>
  	);
  }
}

export default Main;
