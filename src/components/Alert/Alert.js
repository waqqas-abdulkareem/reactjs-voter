import React from 'react';

export const Alert = (props) => {

	let {
		message
	} = props;

	return (
	<div className="alert alert-danger">
        <span >{message}</span>
    </div>)
}

