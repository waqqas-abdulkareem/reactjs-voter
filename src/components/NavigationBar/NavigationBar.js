import React from 'react';

const NavItem = (props) => {
	return (
	<li className="nav-item">
		<a className="nav-link" href={props.href}>{props.label}</a>
	</li>
	)
}

const NavLogoItem = (props) => {
	return (<a className="navbar-brand"  href="/">Voter</a>);
}

class NavigationBar extends React.Component{
	
	render(){
		return (
			<nav className="navbar navbar-expand-lg navbar-dark bg-dark">
			    <NavLogoItem/>
			    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
			        <span className="navbar-toggler-icon"></span>
			    </button>

			    <div className="collapse navbar-collapse" id="navbarColor02">
			        <ul className="navbar-nav mr-auto">
			            <NavItem label="Home" href="/"/>
			            <NavItem label="New Poll" href="/poll"/>
			            <NavItem label="My Polls" href="/polls"/>
			        </ul>
			        <ul className="navbar-nav">
			            <NavItem label="Sign-in" href="/login"/>
			            <NavItem label="Sign-Up" href="/register"/>
			            
			            <li className="nav-item dropdown" >
			                <a className="nav-link dropdown-toggle" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">Profile</a>
			                <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
			                    <div className="dropdown-item" styles={{padding:'0px'}}>
			                        <img  height="100px" id="profile-picture" src="@{/profile/avatar}"/>
			                    </div>
			                    <a className="dropdown-item"  href="/avatar" >Set Profile Picture</a>
			                </div>
			            </li>
			            <NavItem label="Sign out" href="/logout"/>
			        </ul>

			    </div>
			</nav>
		)
	}
}

export default NavigationBar;