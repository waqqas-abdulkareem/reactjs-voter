import React from 'react';
import {connect} from 'react-redux';

import {Alert} from './../Alert/Alert.js'
import {UPDATE_TEXT_FIELD} from './../../constants/actionTypes';

const LoginField = (props) => {

	let {
		type,
		title,
		value,
		placeholder,
		onTextChanged
	} = props

	return (<div className="form-group" >
	    <label >{title}</label>
	    <input type={type} 
	    		onChange={onTextChanged}
	    		className="form-control" 
	    		value={value}
	    		aria-describedby="emailHelp"
	    		placeholder={placeholder} 
	    		required/>
	</div>)
}

const mapDispatchToProps = (dispatch)=>{
	console.log('mapDispatchToProps')
	return {
		onUsernameChanged:(username)=>{
			dispatch({type: UPDATE_TEXT_FIELD,  form: 'login', key: 'email', value: username});
		},
		onPasswordChanged:(password)=>{
			dispatch({type: UPDATE_TEXT_FIELD,  form: 'login', key: 'password', value: password});
		},
		onSubmitLogin:(credentials)=>{
			alert(`Username: ${credentials.email}, Password: ${credentials.password}`);
		}
	}
}

const mapStateToProps = (state)=>{
	console.log(`mapStateToProps: ${JSON.stringify(state,undefined,2)}`);
	return {
		...state.login
	}
}

class Login extends React.Component{

	constructor(props){
		super(props);
		this.onUsernameChanged = (ev) => {
			this.props.onUsernameChanged(ev.target.value);
		}
		this.onPasswordChanged = (ev) => {
			this.props.onPasswordChanged(ev.target.value);
		}
		this.onSubmitLogin = ()=>{
			this.props.onSubmitLogin({
					email: this.props.email,
					password: this.props.password
			});
		}
	}

	render(){
		let alertDiv;
		let {
			email,
			password,
			responseError
		} = this.props;
		console.log(`render email: ${email}, pw: ${password}`)

		if (responseError) {
			alertDiv = <Alert message={responseError}/>
		}

		return (
		<div className="container">
	        <div className="row justify-content-center">
	            <div className="col-4">
	                <h2>Login</h2>
	                <div>
	                	<LoginField 
	                		type="text"
	                		title="Email"
	                		value={email}
	                		placeholder="Enter your e-mail address"
	                		onTextChanged={this.onUsernameChanged}
	                		/>
	                	<LoginField 
	                		type="password"
	                		title="Password"
	                		value={password}
	                		placeholder="Password"
	                		onTextChanged={this.onPasswordChanged}/>
	                    <button 
	                    	type="submit" 
	                    	onClick={this.onSubmitLogin}
	                    	className="btn btn-primary">Submit</button>
	                </div>
	                <br/>
	                {alertDiv}
	            </div>
	        </div>
	    </div>
	    )
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);