import React from 'react';
import {connect} from 'react-redux';
import {UPDATE_TEXT_FIELD} from './../../constants/actionTypes';

const RegistrationField = (props) => {

	let {
		label,
		type,
		placeholder,
		value,
		onTextChanged,
		validationMessage,
	} = props;

	let errorClass = validationMessage? 'is-invalid' : undefined;

	return (<div className="form-group">
        <label >{label}</label>
        <input type={type} 
        		className="form-control"
               errorclass={errorClass}
               placeholder={placeholder}
               onChange={onTextChanged} />
        <div className="invalid-feedback">{validationMessage}
        </div>
    </div>)
}

const mapStateToProps = (state) => {
	return {
		...state.registration
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		onFirstNameChanged: (name)=>{
			dispatch({type: UPDATE_TEXT_FIELD, form: 'registration', key: 'firstName', value: name});
		},
		onLastNameChanged: (name)=>{
			dispatch({type: UPDATE_TEXT_FIELD,  form: 'registration', key: 'lastName', value: name});
		},
		onEmailChanged:(email)=>{
			dispatch({type: UPDATE_TEXT_FIELD,  form: 'registration', key: 'email', value: email});
		},
		onPasswordChanged:(password)=>{
			dispatch({type: UPDATE_TEXT_FIELD,  form: 'registration', key: 'password', value: password});
		},
		onSubmitRegistration:(registration)=>{
			//dispatch({type: SUBMIT_FORM,value: registration});
			alert(`Username: ${registration.email}, Password: ${registration.password}`);
		}
	}
}

class Register extends React.Component{

	constructor(props){
		super(props);
		this.onFirstNameChanged = (ev)=>{
			this.props.onFirstNameChanged(ev.target.value);
		}
		this.onLastNameChanged = (ev)=>{
			this.props.onLastNameChanged(ev.target.value);
		}
		this.onEmailChanged = (ev)=>{
			this.props.onEmailChanged(ev.target.value);
		}
		this.onPasswordChanged = (ev)=>{
			this.props.onPasswordChanged(ev.target.value);
		}
		this.onSubmitRegistration = ()=>{
			this.props.onSubmitRegistration({
				email: this.props.email,
				password: this.props.password,
				firstName: this.props.firstName,
				lastName: this.props.lastName
			});
		}
	}

	render(){
		let {
			firstName,
			lastName,
			email,
			password
		} = this.props;

		return (<div className="container">
	        <div className="row justify-content-center">
	            <div className="col-4">
	                <h2 >Register</h2>
	                <div>
	                    <RegistrationField 
	                    	label="First Name"
	                    	type="text"
	                    	placeholder="First Name"
	                    	value={firstName}
	                    	onTextChanged={this.props.onFirstNameChanged}
	                    />
	                    <RegistrationField 
	                    	label="Last Name"
	                    	type="text"
	                    	placeholder="Last Name"
	                    	value={lastName}
	                    	onTextChanged={this.props.onLastNameChanged}
	                    />
	                    <RegistrationField 
	                    	label="Email address"
	                    	type="email"
	                    	placeholder="Email address"
	                    	value={email}
	                    	onTextChanged={this.onEmailChanged}
	                    />
	                    <RegistrationField 
	                    	label="Password"
	                    	type="password"
	                    	placeholder="Password"
	                    	value={password}
	                    	onTextChanged={this.onPasswordChanged}
	                    />
	                    <button 
	                    	type="submit" 
	                    	className="btn btn-primary" 
	                    	onClick={this.onSubmitRegistration}>Submit</button>
	                </div>
	            </div>
	        </div>
	    </div>
	   )
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Register);