import {UPDATE_TEXT_FIELD} from './../constants/actionTypes'

const registration = (state = {}, action) => {
	//console.log(`register reducer: ${JSON.stringify({action},undefined,2)}`);
	switch(action.type){
		case UPDATE_TEXT_FIELD:
			if (action.form === 'registration') {
				return {
					...state,
					[action.key]: action.value
				}
			}
		default:
			return state;
	}
}

export default registration;