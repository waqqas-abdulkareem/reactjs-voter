import {
	UPDATE_TEXT_FIELD, 
	UPDATE_OPTION_FIELD,
	ADD_OPTION_FIELD
} from './../constants/actionTypes'

const newPoll = (state = {title: "",options: ["",""]}, action) => {
	//console.log(`login reducer: ${JSON.stringify({action},undefined,2)}`);
		console.log(`newPoll reducer: ${JSON.stringify({action},undefined,2)}`)
	switch(action.type){
		case UPDATE_TEXT_FIELD:
			if (action.form === 'newPoll') {
				return {
					...state,
					[action.key]: action.value
				}
			}
		case UPDATE_OPTION_FIELD:
			
			let newOptions = state.options.slice();
			newOptions[action.index] = action.value

			return {
				...state,
				options: newOptions
			}
		case ADD_OPTION_FIELD:
			return {
				...state,
				options: [...state.options,""]
			}
		default:
			return state;
	}
}

export default newPoll;