import {combineReducers} from 'redux';
import login from './login'
import registration from './registration'
import newPoll from './newPoll'

export default combineReducers({
	login,
	registration,
	newPoll
})