import {UPDATE_TEXT_FIELD} from './../constants/actionTypes'

const login = (state = {}, action) => {
	//console.log(`login reducer: ${JSON.stringify({action},undefined,2)}`);
	switch(action.type){
		case UPDATE_TEXT_FIELD:
			if (action.form === 'login') {
				return {
					...state,
					[action.key]: action.value
				}
			}
		default:
			return state;
	}
}

export default login;