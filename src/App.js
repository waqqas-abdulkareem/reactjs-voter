import React, { Component } from 'react';
import {NavigationBar} from './components'
import {Main} from './components'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <NavigationBar/>
        <Main />
      </div>
    );
  }
}

export default App;
